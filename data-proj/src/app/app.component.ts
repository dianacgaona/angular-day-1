import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // declare models for this component
  users:Object[] = []

  // declare functions
  constructor(private http:HttpClient){ 
  }

  getData():Observable<Object[]>{
    return this.http.get<Object[]>('https://jsonplaceholder.typicode.com/users')
  }

  ngOnInit(){
    // make a call for user data
    this.getData().subscribe( (result)=>{
      this.users = result
      console.log(result)} )
  }


}
